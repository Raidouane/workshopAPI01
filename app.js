let express = require('express');
let port = process.env.PORT || 3000;
let bodyParser = require('body-parser');
let logger = require('morgan');
let mongoose = require('mongoose');
mongoose.Promise = global.Promise;

let db = mongoose.connect('mongodb://localhost:27017/authSystem');

let app = express();

app.use(bodyParser.urlencoded({
    extended : false
}));

app.use(logger('dev'));

app.use(bodyParser.json());

app.all('/api/*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
    if (req.method == 'OPTIONS') {
        res.status(200).end();
    } else {

        next();
    }
});

// TODO: HERE YOUR ENDPOINTS
//app.use('/api/sign', require('./src/sign/index'));

app.use((req, res, next) => {

    let err = new Error("URL Request not found");
    err.status = 404;
    res.status(404);
    res.json({
        'error' : 404,
        'response' : 'Ressource non trouvée.'
    });
    next();
});

app.set("port", 3000);

let server = app.listen(app.get('port'), () => {
    console.log('AuthSystemAPI is running on port : ' + server.address().port);
});
